var path         = require('path');
var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var reload       = browserSync.reload;
var del          = require('del');
var Autoprefixer = require('less-plugin-autoprefix');
var minifyCss    = require('gulp-minify-css');
var plumber      = require('gulp-plumber');
var rename       = require('gulp-rename');
var less         = require('gulp-less');
var sourcemaps   = require('gulp-sourcemaps');
var uglify       = require('gulp-uglify');
var concat       = require('gulp-concat');
var merge        = require('merge2');

//var postcss = require('gulp-postcss');
//var mqpacker = require('css-mqpacker');


var config = {
    root: {
        src:  './src',
        dest: './dist'
    },
    html: {
        glob: '/**/*.html'
    },
    less: {
        src:  '/less',
        dest: '/less',
        glob: {
            main: '/*.less',
            all: '/**/*.less'
        },
        includes: '/includes'
    },
    js: {
        src:  '/js',
        dest: '/js',
        glob: {
            all: '/**/*.js',
            min: '/**/*.min.js'
        },
    },
    css: {
        src: '/css',
        dest: '/css',
        glob: {
            all: '/**/*.css',
            min: '/**/*.min.css'
        }
    },
    normalize: {
        path: './bower_components/normalize-css/normalize.css'
    }
};


gulp.task('less-compile', () => {
    var autoprefix = new Autoprefixer({ browsers: ["last 3 versions"]});

    return merge(
        gulp.src(config.normalize.path),
        gulp.src(path.join(config.root.src, config.css.src, config.less.src, config.less.glob.main))
            .pipe(plumber())
            .pipe(less({
                paths: [path.join(config.root.src, config.css.src, config.less.src, config.less.includes)],
                plugins: [autoprefix]
            }))
            //.pipe(postcss([mqpacker]))
            .pipe(gulp.dest(path.join(config.root.src, config.css.src)))
            .on('error', console.error.bind(console))
        )
        .pipe(plumber())
        .pipe(concat('main.css'))
        .pipe(gulp.dest(path.join(config.root.src, config.css.src)));
});


gulp.task('minify-css', () => {
    return gulp.src([
            path.join(config.root.src, config.css.src, config.css.glob.all),
            '!' + path.join(config.root.src, config.css.src, config.css.glob.min)
        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(minifyCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.join(config.root.src, config.css.src)));
});


gulp.task('js-concat', () => {
    return gulp.src([
        path.join(config.root.src, config.js.src, '/components/*.js'),
        path.join(config.root.src, config.js.src, 'main.js')
    ])
    .pipe(plumber())
    .pipe(concat('main.dist.js'))
    .pipe(gulp.dest(path.join(config.root.src, config.js.src)));
});


gulp.task('js-compress', () => {
   return gulp.src([
            path.join(config.root.src, config.js.src, config.js.glob.all),
            '!' + path.join(config.root.src, config.js.src, config.js.glob.min),
            '!' + path.join(config.root.src, config.js.src, '/vendor/**')
        ])
       .pipe(plumber())
       .pipe(sourcemaps.init())
       .pipe(uglify())
       .pipe(rename({suffix: '.min'}))
       .pipe(sourcemaps.write())
       .pipe(gulp.dest(path.join(config.root.src, config.js.src)));
});


//gulp.task('browser-sync', () => {
//    browserSync.init([
//        path.join(config.root.src, config.html.glob),
//        path.join(config.root.src, config.css.src, config.css.glob.all),
//        path.join(config.root.src, config.js.src, config.js.glob.all),
//    ], {
//        server: false,
//        proxy: 'euroshop.local:8080'
//        //server: {
//        //    baseDir: config.root.src
//        //}
//    });
//});


gulp.task('build:create', () => {
    return gulp.src([
            path.join(config.root.src, '/**/*.!(js|css)'),
            path.join(config.root.src, config.css.src, config.css.glob.min),
            path.join(config.root.src, config.js.src, config.js.glob.min),
            path.join(config.root.src, '/**/vendor/**')
        ])
        .pipe(gulp.dest(config.root.dest));
});



//gulp.task('build:start', function() {
//    browserSync({
//        server: {
//            baseDir: config.root.dest
//        }
//    });
//});


gulp.task('build:delete', (res) => {
    return del([config.root.dest+'/**'], res);
});


gulp.task('build', ['less-compile', 'minify-css', 'js-compress', 'build:create']);


gulp.task ('watch', () => {
    gulp.watch(
        path.join(config.root.src, config.css.src, config.less.src, config.less.glob.all),
        ['less-compile']
    );

    gulp.watch(
        path.join(config.root.src, config.js.src, config.js.glob.all),
        ['js-concat']
    );
});


gulp.task('default', ['watch', 'less-compile', 'minify-css', 'js-concat']);