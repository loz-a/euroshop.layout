
+function($) { 'use strict';

    var Carousel = function($element, options) {
        this.$elem          = $element;
        this.$container     = $element.closest('.carousel-container');
        this.$listContainer = this.$container.find('.carousel');
        this.options        = options;

        this.isShowControls();
        if (options.type == 'column') {
           this.adjustHeight();
        }
    };

    Carousel.DEFAULTS = {
        visibleCount: 3,
        duration: 300,
        type: 'column' // or row
    };

    Carousel.prototype.isShowControls = function() {
        if (this.$listContainer.find('li').length > this.options.visibleCount) return true;
        this.$container.find('.navigation').css('display', 'none');
        return false;
    };

    Carousel.prototype.adjustHeight = function() {
        var height = 0;

        this.$listContainer
            .find('li:lt(' + this.options.visibleCount + ')')
            .each(function() {
                height += $(this).outerHeight(true);
            });
        height && this.$listContainer.parent().css('height', height + 'px');
    };

    Carousel.prototype.up = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('top', $liList.last().outerHeight(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({top: 0}, this.options.duration);
    };

    Carousel.prototype.down = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({top: $liList.first().outerHeight(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({top: 0});
                }, this)
            );
    };

    Carousel.prototype.prev = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('margin-left', $liList.outerWidth(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({'margin-left': 0}, this.options.duration);
    };

    Carousel.prototype.next = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({'margin-left': $liList.first().outerWidth(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({'margin-left': 0})
                }, this)
            );
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.carousel');
            var parentData = $this.hasClass('.carousel') ? null : $this.closest('.carousel-container').find('.carousel').data();
            var options = $.extend({}, Carousel.DEFAULTS, typeof parentData == 'object' && parentData, $this.data(), typeof option == 'object' && option);
            var action  = typeof option == 'string' ? option : options.slide;

            if (!data) $this.data('widget.carousel', (data = new Carousel($this, options)));
            if (action) data[action]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = Carousel;


    $(document)
        .on('click.widget.carousel', '.carousel-container .carousel-control', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this, $this.data());
        });

    $(window).on('load', function() {
        $('.carousel').each(function() {
            var $carousel = $(this);
            Plugin.call($carousel, $carousel.data());
        });
    });

}(jQuery);

+function($) {'use strict';

    var $openBtn = null;
    var $dropdown = null;

    function close() {
        if (!$dropdown) return;
        if ($dropdown.hasClass('hidden')) return;

        $dropdown.addClass('hidden');
        $openBtn && $openBtn.attr('aria-expanded', false);
    }

    function open($element) {
        close();
        var $parent = $element.closest('.lang-container');
        $openBtn    = $element.hasClass('open') ? $element : $parent.find('.open');
        $dropdown   = $parent.find('.dropdown-menu');

        if (!$dropdown.hasClass('hidden')) return;
        $dropdown.removeClass('hidden');
        $openBtn.attr('aria-expanded', true);
    }


    $(document)
        .on('click.widget.dropdown-lang', '.lang-container .open', function(evt) {
            evt.stopPropagation();
            open($(this));
        })
        .on('click.widget.dropdown-lang', function(evt) {
            close();
        });

}(jQuery);

+function($) {'use strict';

    var DropdownMultiple = function($element) {
        this.$elem = $element.closest('.dropdown-multiple');
        this.$openElem = $element.hasClass('open') ? $element : this.$elem.find('.open');
        this.$hiddenElem = this.$elem.find('.open+input[type=hidden]');
    };

    DropdownMultiple.prototype.open = function() {
        if (this.$elem.hasClass('open')) return;
        this.$elem.addClass('open');
        this.$openElem.attr('aria-expanded', 'true');
    };

    DropdownMultiple.prototype.close = function() {
        if (this.$elem.hasClass('open')) {
            this.$elem.removeClass("open");
        }
        this.$openElem.attr('aria-expanded', 'false');

        var categoryList = [];
        var idsList = [];
        this.$elem.find('input:checked').each(function() {
            var $this = $(this);
            categoryList.push($this.next('label')[0].innerHTML);
            idsList.push($this.val());
        });
        this.$openElem.val(categoryList.join(', '));
        this.$hiddenElem.val(idsList.join(','));
    };


    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('widget.dropdown-multiple');

            if (!data) $this.data('widget.dropdown-multiple', (data = new DropdownMultiple($this)));
            if (typeof option == 'string') data[option]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = DropdownMultiple;


    $(document)
        .on('click.widget.dropdown-multiple', '.dropdown-multiple .open', function() {
            Plugin.call($(this), 'open');
        })
        .on('click.widget.dropdown-multiple', '.dropdown-multiple .close', function() {
            Plugin.call($(this), 'close');
        });

}(jQuery);

+function($) {'use strict';

    var Dropdown = function($element) {
        this.$elem        = $element;
        this.$container   = $element.closest('.dropdown');
        this.$openBtn     = $element.hasClass('.dropdown-btn') ? $element : this.$container.find('.dropdown-btn');
        this.$openBtnIcon = this.$openBtn.find('.btn-icon');
        this.$hiddenElem  = this.$container.find('.dropdown-btn+input[type=hidden]');
    };

    Dropdown.prototype.open = function() {
        $('.dropdown').removeClass('open');
        this.$container.addClass('open');
    };

    Dropdown.prototype.close = function() {

        var $itemIcon = this.$elem.find('.item-icon').clone();
        this.$openBtn.text(this.$elem.text());
        $itemIcon.insertAfter(this.$openBtn.get(0).firstChild);
        this.$openBtnIcon.insertAfter($itemIcon);

        var value = this.$elem.attr('href').replace('/', '');
        this.$hiddenElem.val(value);

        this.$container.removeClass('open');
    };

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('widget.dropdown');

            if (!data) $this.data('widget.dropdown', (data = new Dropdown($this)));
            if (typeof option == 'string') data[option]();
        });
    }

    $.fn.dropdown             = Plugin;
    $.fn.dropdown.Constructor = Dropdown;


    $(document)
        .on('click.widget.dropdown', '.dropdown .dropdown-btn', function() {
            Plugin.call($(this), 'open');
        })
        .on('click.widget.dropdown', '.dropdown .dropdown-menu a', function(evt) {
            evt.preventDefault();
            Plugin.call($(this), 'close');
        });

}(jQuery);

+function($) { 'use strict';

    var Gallery = function($element) {
        this.$elem      = $element;
        this.$container = $element.closest('.gallery');
        this.$zoomImage = this.$container.find('.zoom-image');
    };

    Gallery.prototype.showImage = function() {
        var zoomImgSrc = this.$elem.attr('href');
        this.$zoomImage.attr('src', zoomImgSrc);

        this.$zoomImage.parent().addClass('preloader');
        this.$zoomImage.load($.proxy(function() {
            this.$zoomImage.parent().removeClass('preloader');
        }, this));
    };


    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.gallery');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('widget.gallery', (data = new Gallery($this)));
            data.showImage();
        });
    }

    $.fn.gallery             = Plugin;
    $.fn.gallery.Constructor = Gallery;


    $(document)
        .on('click.widget.tabs', '.thumbs-list a', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this);
        });

}(jQuery);

+function($) { 'use strict';

    var listViewClass = 'products-list-column';
    var gridViewClass = 'products-list-row';

    var $listContainer = $('.products-list-container');

    $(document)
        .on('click.grid-view', '.list-view-pane a', function(evt) {
            evt.preventDefault();

            var $this       = $(this);
            var targetClass = $this.attr('data-target-class');

            if (targetClass == listViewClass) {
                $listContainer.find('ul').removeClass(gridViewClass).addClass(listViewClass);
            } else if (targetClass == gridViewClass) {
                $listContainer.find('ul').removeClass(listViewClass).addClass(gridViewClass);
            }

            $this.closest('ul').find('a').removeClass('active');
            $this.addClass('active');
        });

}(jQuery);
+function($) { 'use strict';

    var Slider = function($element) {
        this.$elem      = $element;
        this.$container = $element.hasClass('slider') ? $element : $element.closest('.slider');

        this.cycle();
    };

    Slider.interval = null;

    Slider.prototype.go = function(to) {
        var $current = this.$container.find('.active');
        var $target  = $current[to]();

        if (!$target.length) {
            $target = $current.parent().find('li:' + (to == 'next' ? 'first' : 'last'));
        }

        $current.removeClass('active');
        $target.addClass('active fadeIn');

        var timer = setTimeout(function() {
            $target.removeClass('fadeIn');
            clearTimeout(timer);
        }, 1000);

    };

    Slider.prototype.next = function() {
        this.go('next');
    };

    Slider.prototype.prev = function() {
        this.go('prev');
    };

    Slider.prototype.cycle = function() {
        Slider.interval && clearInterval(Slider.interval);
        Slider.interval = setInterval($.proxy(this.next, this), 6000);
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.slider');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);
            var action  = typeof option == 'string' ? option : options.slide;

            if (!data) $this.data('widget.slider', (data = new Slider($this)));
            if (action) data[action]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = Slider;

    $(document)
        .on('click.widget.slider', '.slider .slide-control', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this, $this.data());
        });

    $(window).on('load', function () {
        $('.slider').each(function () {
            var $slider = $(this);
            Plugin.call($slider);
        })
    })

}(jQuery);

+function($) { 'use strict';

    var Tabs = function($element) {
        this.$elem         = $element;
        this.$navContainer = $element.closest('.nav-tabs');
    };

    Tabs.prototype.tab = function() {
        var contentId = this.$elem.attr('href');
        var $targetContentBox = $(contentId);

        this.$navContainer.find('.active').removeClass('active');
        this.$elem.addClass('active');

        $targetContentBox.closest('.tab-content').find('.active').removeClass('active');
        $targetContentBox.addClass('active');
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.tabs');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('widget.tabs', (data = new Tabs($this)));
            data.tab();
        });
    }

    $.fn.tabs             = Plugin;
    $.fn.tabs.Constructor = Tabs;


    $(document)
        .on('click.widget.tabs', '.nav-tabs a', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this);
        });

}(jQuery);
$(function() {
    console.log('hello in main');
});