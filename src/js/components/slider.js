+function($) { 'use strict';

    var Slider = function($element) {
        this.$elem      = $element;
        this.$container = $element.hasClass('slider') ? $element : $element.closest('.slider');

        this.cycle();
    };

    Slider.interval = null;

    Slider.prototype.go = function(to) {
        var $current = this.$container.find('.active');
        var $target  = $current[to]();

        if (!$target.length) {
            $target = $current.parent().find('li:' + (to == 'next' ? 'first' : 'last'));
        }

        $current.removeClass('active');
        $target.addClass('active fadeIn');

        var timer = setTimeout(function() {
            $target.removeClass('fadeIn');
            clearTimeout(timer);
        }, 1000);

    };

    Slider.prototype.next = function() {
        this.go('next');
    };

    Slider.prototype.prev = function() {
        this.go('prev');
    };

    Slider.prototype.cycle = function() {
        Slider.interval && clearInterval(Slider.interval);
        Slider.interval = setInterval($.proxy(this.next, this), 6000);
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.slider');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);
            var action  = typeof option == 'string' ? option : options.slide;

            if (!data) $this.data('widget.slider', (data = new Slider($this)));
            if (action) data[action]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = Slider;

    $(document)
        .on('click.widget.slider', '.slider .slide-control', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this, $this.data());
        });

    $(window).on('load', function () {
        $('.slider').each(function () {
            var $slider = $(this);
            Plugin.call($slider);
        })
    })

}(jQuery);