
+function($) {'use strict';

    var $openBtn = null;
    var $dropdown = null;

    function close() {
        if (!$dropdown) return;
        if ($dropdown.hasClass('hidden')) return;

        $dropdown.addClass('hidden');
        $openBtn && $openBtn.attr('aria-expanded', false);
    }

    function open($element) {
        close();
        var $parent = $element.closest('.lang-container');
        $openBtn    = $element.hasClass('open') ? $element : $parent.find('.open');
        $dropdown   = $parent.find('.dropdown-menu');

        if (!$dropdown.hasClass('hidden')) return;
        $dropdown.removeClass('hidden');
        $openBtn.attr('aria-expanded', true);
    }


    $(document)
        .on('click.widget.dropdown-lang', '.lang-container .open', function(evt) {
            evt.stopPropagation();
            open($(this));
        })
        .on('click.widget.dropdown-lang', function(evt) {
            close();
        });

}(jQuery);