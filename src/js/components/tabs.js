
+function($) { 'use strict';

    var Tabs = function($element) {
        this.$elem         = $element;
        this.$navContainer = $element.closest('.nav-tabs');
    };

    Tabs.prototype.tab = function() {
        var contentId = this.$elem.attr('href');
        var $targetContentBox = $(contentId);

        this.$navContainer.find('.active').removeClass('active');
        this.$elem.addClass('active');

        $targetContentBox.closest('.tab-content').find('.active').removeClass('active');
        $targetContentBox.addClass('active');
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.tabs');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('widget.tabs', (data = new Tabs($this)));
            data.tab();
        });
    }

    $.fn.tabs             = Plugin;
    $.fn.tabs.Constructor = Tabs;


    $(document)
        .on('click.widget.tabs', '.nav-tabs a', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this);
        });

}(jQuery);