
+function($) { 'use strict';

    var Carousel = function($element, options) {
        this.$elem          = $element;
        this.$container     = $element.closest('.carousel-container');
        this.$listContainer = this.$container.find('.carousel');
        this.options        = options;

        this.isShowControls();
        if (options.type == 'column') {
           this.adjustHeight();
        }
    };

    Carousel.DEFAULTS = {
        visibleCount: 3,
        duration: 300,
        type: 'column' // or row
    };

    Carousel.prototype.isShowControls = function() {
        if (this.$listContainer.find('li').length > this.options.visibleCount) return true;
        this.$container.find('.navigation').css('display', 'none');
        return false;
    };

    Carousel.prototype.adjustHeight = function() {
        var height = 0;

        this.$listContainer
            .find('li:lt(' + this.options.visibleCount + ')')
            .each(function() {
                height += $(this).outerHeight(true);
            });
        height && this.$listContainer.parent().css('height', height + 'px');
    };

    Carousel.prototype.up = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('top', $liList.last().outerHeight(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({top: 0}, this.options.duration);
    };

    Carousel.prototype.down = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({top: $liList.first().outerHeight(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({top: 0});
                }, this)
            );
    };

    Carousel.prototype.prev = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('margin-left', $liList.outerWidth(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({'margin-left': 0}, this.options.duration);
    };

    Carousel.prototype.next = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({'margin-left': $liList.first().outerWidth(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({'margin-left': 0})
                }, this)
            );
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.carousel');
            var parentData = $this.hasClass('.carousel') ? null : $this.closest('.carousel-container').find('.carousel').data();
            var options = $.extend({}, Carousel.DEFAULTS, typeof parentData == 'object' && parentData, $this.data(), typeof option == 'object' && option);
            var action  = typeof option == 'string' ? option : options.slide;

            if (!data) $this.data('widget.carousel', (data = new Carousel($this, options)));
            if (action) data[action]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = Carousel;


    $(document)
        .on('click.widget.carousel', '.carousel-container .carousel-control', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this, $this.data());
        });

    $(window).on('load', function() {
        $('.carousel').each(function() {
            var $carousel = $(this);
            Plugin.call($carousel, $carousel.data());
        });
    });

}(jQuery);