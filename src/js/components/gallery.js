
+function($) { 'use strict';

    var Gallery = function($element) {
        this.$elem      = $element;
        this.$container = $element.closest('.gallery');
        this.$zoomImage = this.$container.find('.zoom-image');
    };

    Gallery.prototype.showImage = function() {
        var zoomImgSrc = this.$elem.attr('href');
        this.$zoomImage.attr('src', zoomImgSrc);

        this.$zoomImage.parent().addClass('preloader');
        this.$zoomImage.load($.proxy(function() {
            this.$zoomImage.parent().removeClass('preloader');
        }, this));
    };


    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.gallery');
            var options = $.extend({}, $this.data(), typeof option == 'object' && option);

            if (!data) $this.data('widget.gallery', (data = new Gallery($this)));
            data.showImage();
        });
    }

    $.fn.gallery             = Plugin;
    $.fn.gallery.Constructor = Gallery;


    $(document)
        .on('click.widget.tabs', '.thumbs-list a', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this);
        });

}(jQuery);